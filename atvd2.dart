import 'Dart:math';
import 'dart:io';

//2) Gerar números de 1 a 100 e pedir um número para o usuário adivinhar e dizer se o valor é maior, menor ou se acertou
void main() {
  var rng = Random();
  var value = rng.nextInt(100);
  bool loop = true;

  while (loop) {
    print("digite um valor para adivinhar\n");
    int entry = int.parse(stdin.readLineSync()!);

    if (entry > value) print("o valor digitado é maior\n");
    if (entry < value) print("o valor digitado é menor\n");
    if (entry == value) {
      print("Você acertou!\n");
      loop = false;
    }
  }
}
