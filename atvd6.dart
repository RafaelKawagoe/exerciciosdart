import 'Dart:math';
import 'dart:io';
//6) Faça o jogo do pedra papel e tesoura. Peça ao usuário uma opção e diga se ele ganhou

void main() {
  Map rules = {
    '1': 'pedra',
    '2': 'papel',
    '3': 'tesoura',
  };
  stdout.writeln('Escolha um:\n1-Pedra\n2-Papel\n3-Tesoura\n');
  var entry = stdin.readLineSync();
  var rng = Random();
  var value = (rng.nextInt(2) + 1).toString();
  print("A máquina escolheu: " + rules[value]);

  switch (entry) {
    case '1':
      rules[value] == 'pedra'
          ? print("empate")
          : rules[value] == 'papel'
              ? print("perde")
              : print("ganhou");
      break;
    case '2':
      rules[value] == 'pedra'
          ? print("ganhou")
          : rules[value] == 'papel'
              ? print("empate")
              : print("perdeu");
      break;
    case '3':
      rules[value] == 'pedra'
          ? print("perdeu")
          : rules[value] == 'papel'
              ? print("ganhou")
              : print("empate");
      break;
    default:
  }
}
